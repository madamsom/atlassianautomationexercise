package mypackage;

import java.util.concurrent.TimeUnit;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class myclass {
	
	//Prerequisites
	public static String myUserName = ""; //Username to login 
	public static String myPassword = ""; //Password to login
	public static String myIssue = ""; //Issues summary 
	
	public static WebDriver driver;
	
    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get("https://jira.atlassian.com/browse/TST");
    	driver.findElement(By.cssSelector("a[href*='/login.jsp?os_destination=%2Fbrowse%2FTST']")).click();
    	mylogin (myUserName, myPassword);
    	driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
    }
    
    @Test
    public void testIssue() throws Exception {
    	//Create an issue
    	driver.findElement(By.id("create_link")).click();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);

		driver.findElement(By.id("summary")).sendKeys(myIssue);  
		driver.findElement(By.id("create-issue-submit")).click();
		
		//Search an issue
    	driver.findElement(By.id("quickSearchInput")).click();
    	driver.findElement(By.id("quickSearchInput")).sendKeys(myIssue);
    	driver.findElement(By.id("quickSearchInput")).sendKeys(Keys.ENTER);
    	driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
    	
    	Assert.assertThat("Test Create and Search", 
    			driver.findElement(By.className("issue-link-summary")).getText(), 
    			Matchers.containsString(myIssue));
		  
		//Update an issue
    	driver.findElement(By.className("splitview-issue-link")).click();
		driver.findElement(By.id("key-val")).click();
		driver.findElement(By.id("edit-issue")).click();
		  
		WebElement summaryTxt = driver.findElement(By.id("summary"));
		String previousText = summaryTxt.getAttribute("value");
		summaryTxt.clear();
		String updatedSummary = previousText + " - Updated";
		summaryTxt.sendKeys(updatedSummary);
		  
		driver.findElement(By.id("edit-issue-submit")).click();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
		
		Assert.assertThat("Test Update", 
				driver.findElement(By.id("summary-val")).getText(), 
				Matchers.comparesEqualTo(updatedSummary));
    }
    
    @After
    public void tearDown() throws Exception {
        driver.close();
    }
	
	private static void mylogin(String username, String password){
		driver.findElement(By.cssSelector("input[id=username]")).sendKeys(username);
		driver.findElement(By.cssSelector("input[id=password]")).sendKeys(password);
		driver.findElement(By.cssSelector("input[id=login-submit]")).click();
		}
	}
